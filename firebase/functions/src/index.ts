import { ethers } from 'ethers'
import { initializeApp } from 'firebase-admin/app'
import { Filter, Timestamp, getFirestore } from 'firebase-admin/firestore'
import { MoonCatRescue, MoonCatAcclimator, JumpPort } from '@mooncatrescue/contracts/moonCatUtils'
import { EVENTS_COLLECTION, saveContractLogs } from './logUtils'
import { MOONCAT_COLLECTION, parseMoonCatIdentifier } from './moonCatUtils'
import { MOMENTS_ADDRESS } from './momentUtils'

/**
 * Create Firestore Cloud Functions
 *
 * This is the main entrypoint for creating Functions that will be deployed to Firebase.
 * All functions that are export from this file will be found by a `firebase deploy` call and turned
 * into Cloud Functions.
 */

// The Cloud Functions for Firebase SDK. Used to create Cloud Functions and set up triggers.
import { onRequest } from 'firebase-functions/v2/https'
import { logger } from 'firebase-functions'

// The Firebase Admin SDK to access Firestore.
initializeApp()
const db = getFirestore()

// Bring in API endpoint definitions from other files
// These need to be done after the Firebase Admin SDK is initialized; they each assume the app is already initialized, to access the Firestore hooks
import { getOwnerProfile, getOwnerProfiles, updateAddressMetadata } from './ownerProfiles'
import { getMoonCat, onMoonCatUpdated } from './moonCats'
import { onMomentUpdated } from './moments'

/**
 * Fetch the most-recent MoonCatRescue events and save them to Firebase
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const updateMoonCatRescueEvents = onRequest({ secrets: ['API_URL'], timeoutSeconds: 540 }, async (req, res) => {
  switch (req.method) {
    case 'GET':
      const provider = new ethers.providers.JsonRpcProvider(process.env.API_URL)
      const rs = await saveContractLogs({
        provider: provider,
        db: db,
        taskName: 'mooncatrescue-events-catchup',
        contract: MoonCatRescue,
      })
      res.status(rs.code).json(rs)
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})

/**
 * Fetch the most-recent MoonCatAcclimator events and save them to Firebase
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const updateMoonCatAcclimatorEvents = onRequest(
  { secrets: ['API_URL'], timeoutSeconds: 540 },
  async (req, res) => {
    switch (req.method) {
      case 'GET':
        const provider = new ethers.providers.JsonRpcProvider(process.env.API_URL)
        const rs = await saveContractLogs({
          provider: provider,
          db: db,
          taskName: 'mooncatacclimator-events-catchup',
          contract: MoonCatAcclimator,
        })
        res.status(rs.code).json(rs)
        return
      case 'OPTIONS':
        res.setHeader('Allow', ['GET'])
        res.status(204).end()
        return
      default:
        res.setHeader('Allow', ['GET'])
        res.status(405).end()
        return
    }
  }
)

/**
 * Fetch the most-recent events from other MoonCatRescue-related contracts and save them to Firebase
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const updateOtherEvents = onRequest({ secrets: ['API_URL'], timeoutSeconds: 540 }, async (req, res) => {
  switch (req.method) {
    case 'GET':
      const provider = new ethers.providers.JsonRpcProvider(process.env.API_URL)

      // JumpPort events
      let rs = await saveContractLogs({
        provider: provider,
        db: db,
        taskName: 'jumpport-events-catchup',
        contract: JumpPort,
      })
      if (rs.code != 200) {
        res.status(rs.code).json(rs)
        return
      }

      // Moments events
      rs = await saveContractLogs({
        provider: provider,
        db: db,
        taskName: 'moments-events-catchup',
        contract: new ethers.Contract(MOMENTS_ADDRESS, [
          'event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)',
          'event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)',
          'event ApprovalForAll(address indexed owner, address indexed operator, bool approved)',
        ]),
      })

      res.status(rs.code).json(rs)
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})

/**
 * Check for updated metadata information about individual Ethereum Addresses
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const updateAddressMeta = onRequest({ secrets: ['API_URL'], timeoutSeconds: 540 }, async (req, res) => {
  switch (req.method) {
    case 'GET':
      const provider = new ethers.providers.JsonRpcProvider(process.env.API_URL)
      const rs = await updateAddressMetadata(provider)

      res.status(rs.code).json(rs)
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})

/**
 * Retrieve blockchain events.
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const getBlockchainEvents = onRequest(async (req, res) => {
  switch (req.method) {
    case 'GET':
      let limit = 10
      let contract: string | false = false
      let eventName: string | false = false
      let address: string | false = false
      let moonCatRescueOrder: number | false = false
      let moonCatHexId: string | false = false
      let startAfterBlock: number | false = false
      let startAfterLog: number | false = false

      // Check input parameters for errors
      let errors = []
      if (typeof req.query.limit != 'undefined') {
        if (
          typeof req.query.limit != 'string' ||
          isNaN(Number(req.query.limit)) ||
          parseInt(req.query.limit) < 1 ||
          parseInt(req.query.limit) > 500
        ) {
          errors.push('limit')
        } else {
          // Query parameter is valid; parse it
          limit = parseInt(req.query.limit)
        }
      }
      if (typeof req.query.contract != 'undefined') {
        if (typeof req.query.contract != 'string' || !ethers.utils.isAddress(req.query.contract)) {
          errors.push('contract')
        } else {
          // Query parameter is valid; parse it
          contract = ethers.utils.getAddress(req.query.contract)
        }
      }
      if (typeof req.query.event != 'undefined') {
        if (typeof req.query.event != 'string') {
          errors.push('event')
        } else {
          // Query parameter is valid; parse it
          eventName = req.query.event as string
        }
      }
      if (typeof req.query.address != 'undefined') {
        if (typeof req.query.address != 'string' || !ethers.utils.isAddress(req.query.address)) {
          errors.push('address')
        } else {
          // Query parameter is valid; parse it
          address = ethers.utils.getAddress(req.query.address)
        }
      }
      if (typeof req.query.mooncat != 'undefined') {
        if (typeof req.query.mooncat != 'string' || isNaN(Number(req.query.mooncat))) {
          errors.push('mooncat')
        } else {
          // String passed as MoonCat identifier to filter on; determine if it's a rescue order or hex ID, and look up the other
          const rs = await parseMoonCatIdentifier(db.collection(MOONCAT_COLLECTION), req.query.mooncat)
          moonCatRescueOrder = rs.rescueOrder
          moonCatHexId = rs.hexId
        }
      }

      if (typeof req.query.start_after != 'undefined') {
        if (typeof req.query.start_after != 'string') {
          errors.push('start_after')
        } else {
          // startAfter is a string; verify its format
          let [block, log] = req.query.start_after.split('-')
          if (block == '' || isNaN(Number(block)) || log == '' || isNaN(Number(log))) {
            errors.push('start_after')
          } else {
            // Query parameter is valid; parse it
            startAfterBlock = parseInt(block)
            startAfterLog = parseInt(log)
          }
        }
      }
      if (errors.length > 0) {
        res.status(400).json({ ok: false, error: 'Invalid ' + errors.join(', ') })
        return
      }

      // Query for events
      let query = db
        .collection(EVENTS_COLLECTION)
        .orderBy('blockNumber', 'desc')
        .orderBy('logIndex', 'desc')
        .limit(limit)

      // Limit to a specific smart contract's events if `contract` query param is set
      if (contract) {
        logger.debug(`Filter events for eventContract == ${contract}`)
        query = query.where('eventContract', '==', contract)
      }

      // Limit to a specific event name if `event` query param is set
      if (eventName) {
        logger.debug(`Filter events for eventName == ${eventName}`)
        query = query.where('eventName', '==', eventName)
      }

      // Limit to a specific address if `address` query param is set
      if (address) {
        logger.debug(`Filter events for address == ${address}`)
        query = query.where(
          Filter.or(
            Filter.where('tx.from', '==', address),
            Filter.where('tx.to', '==', address),
            Filter.where('args.sender', '==', address),
            Filter.where('args.account', '==', address),
            Filter.where('args.owner', '==', address),
            Filter.where('args.from', '==', address),
            Filter.where('args.to', '==', address)
          )
        )
      }

      // Limit to a specific MoonCat if `mooncat` query param is set
      if (moonCatRescueOrder !== false && moonCatHexId !== false) {
        // MoonCat rescue order and Hex ID are known; filter on all holding contracts
        logger.debug(`Filter events for MoonCat ${moonCatRescueOrder} (${moonCatHexId})`)
        query = query.where(
          Filter.or(
            Filter.and(
              // JumpPort-emitted events
              Filter.where('args.tokenId', '==', moonCatRescueOrder),
              Filter.where('args.tokenAddress', '==', MoonCatAcclimator.address),
              Filter.where('eventContract', '==', JumpPort.address)
            ),
            Filter.and(
              // Acclimator-emitted events
              Filter.where('args.tokenId', '==', moonCatRescueOrder),
              Filter.where('eventContract', '==', MoonCatAcclimator.address)
            ),
            Filter.and(
              // MoonCatRescue rescue events
              Filter.where('args.catId', '==', moonCatHexId),
              Filter.where('eventContract', '==', MoonCatRescue.address)
            ),
            Filter.and(
              // MoonCatRescue Genesis batch events
              Filter.where('args.catIds', 'array-contains', moonCatHexId),
              Filter.where('eventContract', '==', MoonCatRescue.address)
            )
          )
        )
      } else if (moonCatRescueOrder !== false) {
        // Only rescue order is known
        logger.debug(`Filter events for MoonCat ${moonCatRescueOrder}`)
        query = query.where(
          Filter.or(
            Filter.and(
              // JumpPort-emitted events
              Filter.where('args.tokenId', '==', moonCatRescueOrder),
              Filter.where('args.tokenAddress', '==', MoonCatAcclimator.address),
              Filter.where('eventContract', '==', JumpPort.address)
            ),
            Filter.and(
              // Acclimator-emitted events
              Filter.where('args.tokenId', '==', moonCatRescueOrder),
              Filter.where('eventContract', '==', MoonCatAcclimator.address)
            )
          )
        )
      } else if (moonCatHexId !== false) {
        // Only Hex ID is known
        logger.debug(`Filter events for MoonCat ${moonCatHexId}`)
        query = query.where(
          Filter.or(
            Filter.and(
              // MoonCatRescue rescue events
              Filter.where('args.catId', '==', moonCatHexId),
              Filter.where('eventContract', '==', MoonCatRescue.address)
            ),
            Filter.and(
              // MoonCatRescue Genesis batch events
              Filter.where('args.catIds', 'array-contains', moonCatHexId),
              Filter.where('eventContract', '==', MoonCatRescue.address)
            )
          )
        )
      }

      // Start at a specific offset, if `start_after` query param is set
      if (startAfterBlock) {
        logger.debug(`Filter event starting after ${startAfterBlock}-${startAfterLog}`)
        query = query.startAfter(startAfterBlock, startAfterLog)
      }

      // Return result
      const snapshot = await query.get()
      res.json(
        snapshot.docs.map((eventSnap) => {
          const data = eventSnap.data()
          data.timestamp = (data.timestamp as Timestamp).seconds
          return data
        })
      )
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})

// Export API endpoint definitions that are defined in other files
exports.getOwnerProfile = getOwnerProfile
exports.getOwnerProfiles = getOwnerProfiles
exports.onMoonCatUpdated = onMoonCatUpdated
exports.onMomentUpdated = onMomentUpdated
exports.getMoonCat = getMoonCat
