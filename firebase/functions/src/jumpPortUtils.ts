import { logger } from 'firebase-functions'
import { DocumentSnapshot, FieldValue } from 'firebase-admin/firestore'
import { EventUpdateListener, shouldUpdateBlockchainMoment } from './logUtils'
import { BlockchainMoment, HexString } from './types'
import { JumpPort, MoonCatAcclimator } from '@mooncatrescue/contracts/moonCatUtils'
import { MOONCAT_COLLECTION } from './moonCatUtils'
import { MOMENTS_ADDRESS, MOMENTS_COLLECTION } from './momentUtils'

/**
 * Update dependent documents for Deposit Events
 *
 * Called when a Deposit Event from the JumpPort contract is about to be saved to Firestore.
 * The Event has not been saved yet; this function is called within the same Transaction, and will be
 * saved atomically with the Event.
 */
export const postDeposit: EventUpdateListener = async function (db, dbtx, evtLog) {
  const { owner, tokenAddress, tokenId } = evtLog.args as {
    owner: HexString
    tokenAddress: HexString
    tokenId: number
  }

  let snapshot: DocumentSnapshot | false = false
  let docLabel = ''

  switch (tokenAddress) {
    case MoonCatAcclimator.address: {
      // MoonCat got deposited into the JumpPort
      const rescueOrder = tokenId
      const qs = await dbtx.get(db.collection(MOONCAT_COLLECTION).where('rescueOrder', '==', rescueOrder).limit(1))
      if (qs.empty) {
        logger.error(`Deposit event seen for MoonCat ${rescueOrder}, but no trait document exists for that MoonCat`)
        return
      }
      docLabel = 'MoonCat ' + rescueOrder
      snapshot = qs.docs[0]
      break
    }
    case MOMENTS_ADDRESS: {
      // Moment got deposited into the JumpPort
      snapshot = await dbtx.get(db.collection(MOMENTS_COLLECTION).doc(String(tokenId)))
      if (!snapshot.exists) {
        logger.error(`Deposit event seen for Moment ${tokenId}, but no trait document exists for that Moment`)
        return
      }
      docLabel = 'Moment ' + tokenId
      break
    }
  }

  if (snapshot === false) return

  const logPreamble = `Deposit event seen for ${docLabel}`
  const docData = snapshot.data()
  if (typeof docData == 'undefined') {
    logger.error(`${logPreamble}, but trait document is empty`)
    return
  }
  const { shouldUpdateValue, shouldUpdateModified, shouldClearChecked } = shouldUpdateBlockchainMoment(
    docData.owner[JumpPort.address],
    evtLog.blockNumber
  )
  if (!shouldUpdateValue && !shouldUpdateModified && !shouldClearChecked) {
    logger.debug(
      `${logPreamble}, owned by ${owner}, but no update needed for that: ${JSON.stringify(
        docData.owner[JumpPort.address]
      )}`
    )
    return
  }

  // Assemble an object for feeding into a Firestore "update" call, for this change
  let updateDoc: { [key: string]: any } = {}
  const eventModified: BlockchainMoment = {
    blockHeight: evtLog.blockNumber,
    timestamp: evtLog.timestamp,
    txHash: evtLog.tx.hash,
  }
  if (shouldClearChecked) updateDoc[`owner.${JumpPort.address}.checked`] = FieldValue.delete()
  if (shouldUpdateValue) updateDoc[`owner.${JumpPort.address}.value`] = owner
  if (shouldUpdateModified) updateDoc[`owner.${JumpPort.address}.modified`] = eventModified
  await dbtx.update(snapshot.ref, updateDoc)
  if (shouldUpdateValue) {
    logger.info(`${logPreamble}, owned by ${owner}. Metadata updated.`)
  } else if (shouldUpdateModified) {
    logger.debug(`${logPreamble}, owned by ${owner}. Updating last-modified record.`)
  }
}

/**
 * Update dependent documents for Withdraw Events
 *
 * Called when a Withdraw Event from the JumpPort contract is about to be saved to Firestore.
 * The Event has not been saved yet; this function is called within the same Transaction, and will be
 * saved atomically with the Event.
 */
export const postWithdraw: EventUpdateListener = async function (db, dbtx, evtLog) {
  const { tokenAddress, tokenId } = evtLog.args as {
    tokenAddress: HexString
    tokenId: number
  }

  let snapshot: DocumentSnapshot | false = false
  let docLabel = ''

  switch (tokenAddress) {
    case MoonCatAcclimator.address: {
      // MoonCat got withdrawn from the JumpPort
      const rescueOrder = tokenId
      const qs = await dbtx.get(db.collection(MOONCAT_COLLECTION).where('rescueOrder', '==', rescueOrder).limit(1))
      if (qs.empty) {
        logger.error(`Withdraw event seen for MoonCat ${rescueOrder}, but no trait document exists for that MoonCat`)
        return
      }
      snapshot = qs.docs[0]
      docLabel = 'MoonCat ' + rescueOrder
      break
    }
    case MOMENTS_ADDRESS: {
      // Moment got withdrawn into the JumpPort
      snapshot = await dbtx.get(db.collection(MOMENTS_COLLECTION).doc(String(tokenId)))
      if (!snapshot.exists) {
        logger.error(`Withdraw event seen for Moment ${tokenId}, but no trait document exists for that Moment`)
        return
      }
      docLabel = 'Moment ' + tokenId
      break
    }
  }

  if (snapshot === false) return

  const logPreamble = `Withdraw event seen for ${docLabel}`
  const docData = snapshot.data()
  if (typeof docData == 'undefined') {
    logger.error(`${logPreamble}, but trait document is empty`)
    return
  }

  const { shouldUpdateValue, shouldUpdateModified, shouldClearChecked } = shouldUpdateBlockchainMoment(
    docData.owner[JumpPort.address],
    evtLog.blockNumber
  )
  if (!shouldUpdateValue && !shouldUpdateModified && !shouldClearChecked) {
    logger.debug(`${logPreamble}, but no update needed for that: ${JSON.stringify(docData.owner[JumpPort.address])}`)
    return
  }

  // Assemble an object for feeding into a Firestore "update" call, for this change
  let updateDoc: { [key: string]: any } = {}
  const eventModified: BlockchainMoment = {
    blockHeight: evtLog.blockNumber,
    timestamp: evtLog.timestamp,
    txHash: evtLog.tx.hash,
  }
  if (shouldClearChecked) updateDoc[`owner.${JumpPort.address}.checked`] = FieldValue.delete()
  if (shouldUpdateValue) updateDoc[`owner.${JumpPort.address}.value`] = false
  if (shouldUpdateModified) updateDoc[`owner.${JumpPort.address}.modified`] = eventModified
  await dbtx.update(snapshot.ref, updateDoc)
  if (shouldUpdateValue) {
    logger.info(`${logPreamble}. MoonCat metadata updated.`)
  } else if (shouldUpdateModified) {
    logger.debug(`${logPreamble}. Updating last-modified record.`)
  }
}
