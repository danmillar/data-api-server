import { ethers } from 'ethers'
import { DocumentSnapshot, FieldPath, getFirestore, Timestamp } from 'firebase-admin/firestore'
import { ADDR_META_COLLECTION } from './logUtils'
import { getMoonCatOwnerFromDoc, MOONCAT_COLLECTION, MoonCatSummary } from './moonCatUtils'
import { getPathSuffix } from './utils'

// The Cloud Functions for Firebase SDK. Used to create Cloud Functions and set up triggers.
import { onRequest, Request } from 'firebase-functions/v2/https'
import { logger } from 'firebase-functions'
import { BlockchainValue, HexString } from './types'
import { JumpPort, MoonCatAcclimator } from '@mooncatrescue/contracts/moonCatUtils'

// The Firebase Admin SDK to access Firestore.
const db = getFirestore()

/**
 * Analyze an incoming user request and parse out the requested Ethereum Address in it
 *
 * Grabs the "address" query parameter if it's present. If that's not present, uses URL inspection to grab it from the end of the URL.
 * If an address is specified, assume it's a hex ID and attempt to normalize it. If that fails, assume it's an ENS name and attempt
 * to resolve it to an address. If unable to resolve the input to a normalized hex address ID, returns null.
 */
export async function parseInputAddress(req: Request, prefix: string, provider: ethers.providers.JsonRpcProvider) {
  let address = req.query.address
  if (!address || Array.isArray(address)) {
    // Fetch from path rather than query string
    address = getPathSuffix(req, prefix)
    if (!address) return null
  }
  try {
    // Attempt to normalize the input as if it were a hex address ID and return it.
    return ethers.utils.getAddress((address as string).toLowerCase())
  } catch (err) {
    if ((err as any).code !== 'INVALID_ARGUMENT') {
      // Unknown/unexpected error
      logger.error('Ethers address parsing error', err)
      return null
    }
    // Expected parsing error for not an address. Try ENS resolution instead:
    let parsedAddress = await provider.resolveName(address as string)
    if (parsedAddress == null) {
      logger.error(`Failed to parse '${address}' as an ENS address`)
      return null
    }
    return parsedAddress
  }
}

/**
 * Helper for onDocumentWritten handlers
 *
 * Takes in before and after document snapshots and does a comparison to see if ownership information
 * changed with this write, and therefore the metadata about the owner it's going from and the owner
 * it's going to need to be updated.
 */
export async function parseOwnershipUpdates(
  docLabel: string,
  after: DocumentSnapshot,
  before: DocumentSnapshot,
  ownershipParser: (doc: DocumentSnapshot) => BlockchainValue<HexString> | null
) {
  let newOwner: BlockchainValue<HexString> | null
  try {
    newOwner = ownershipParser(after)
  } catch (err) {
    logger.error('Unknown owner', docLabel, after.get('owner'))
    return
  }
  if (newOwner?.value == MoonCatAcclimator.address) {
    // Check if owned as an ERC998 child object
    const new998Token = after.get(`owner.${MoonCatAcclimator.address}-998.value`)
    if (typeof new998Token != 'undefined' && new998Token !== false) {
      // Look up the owner of the MoonCat this asset is the child of
      const snapshot = await db.collection(MOONCAT_COLLECTION).where('rescueOrder', '==', new998Token).limit(1).get()
      if (!snapshot.empty) {
        newOwner = getMoonCatOwnerFromDoc(snapshot.docs[0])
        logger.debug(
          `${docLabel} is an ERC998 child token of MoonCat ${new998Token}, which is owned by ${newOwner?.value}`
        )
      } else {
        logger.debug(
          `${docLabel} is an ERC998 child token of MoonCat ${new998Token}, but there's no MoonCat with that rescue order`
        )
      }
    }
  }

  let oldOwner: BlockchainValue<HexString> | null
  try {
    oldOwner = ownershipParser(before)
  } catch (err) {
    logger.error('Unknown owner', docLabel, before.get('owner'))
    return
  }
  if (oldOwner?.value == MoonCatAcclimator.address) {
    // Check if owned as an ERC998 child object
    const old998Token = before.get(`owner.${MoonCatAcclimator.address}-998.value`)
    if (typeof old998Token != 'undefined' && old998Token !== false) {
      // Look up the owner of the MoonCat this asset was the child of
      const snapshot = await db.collection(MOONCAT_COLLECTION).where('rescueOrder', '==', old998Token).limit(1).get()
      if (!snapshot.empty) {
        oldOwner = getMoonCatOwnerFromDoc(snapshot.docs[0])
        logger.debug(
          `${docLabel} was an ERC998 child token of MoonCat ${old998Token}, which is owned by ${oldOwner?.value}`
        )
      } else {
        logger.debug(
          `${docLabel} was an ERC998 child token of MoonCat ${old998Token}, but there's no MoonCat with that rescue order`
        )
      }
    }
  }

  if (newOwner == null && oldOwner == null) return

  const oldJumpPortOwner = before.get(`owner.${JumpPort.address}.value`)
  const newJumpPortOwner = after.get(`owner.${JumpPort.address}.value`)

  const ownersNeedingUpdates: { [address: HexString]: boolean } = {}
  if (oldOwner != null && oldOwner!.value !== newOwner?.value) {
    // Ownership value changed, and old value was a non-empty value; it should be updated
    ownersNeedingUpdates[oldOwner.value] = true
  }
  if (
    newOwner != null && // MoonCat has an owner value, and...
    (oldOwner?.value !== newOwner.value || // it changed, or...
      (newJumpPortOwner == false && oldJumpPortOwner != false) || // MoonCat was withdrawn from JumpPort, or...
      (newJumpPortOwner != false && oldJumpPortOwner == false)) // MoonCat was deposited into JumpPort
  ) {
    ownersNeedingUpdates[newOwner.value] = true
  }
  logger.debug(`${docLabel} document updated: ownership from ${oldOwner?.value} -> ${newOwner?.value}`)
  return ownersNeedingUpdates
}

/**
 * Update metadata about individual Ethereum Addresses that need it
 */
export async function updateAddressMetadata(
  provider: ethers.providers.JsonRpcProvider
): Promise<{ ok: boolean; code: number; [key: string]: any }> {
  let addressesProcessed = 0

  // Find Addresses that don't have any metadata yet
  const snapshot = await db.collection(ADDR_META_COLLECTION).where('checked', '==', null).limit(50).get()

  const latestBlock = await provider.getBlock('latest')
  const checked = {
    blockHeight: latestBlock.number,
    timestamp: Timestamp.fromMillis(latestBlock.timestamp * 1000),
  }
  const batch = db.batch()
  for (const addrMeta of snapshot.docs) {
    const code = await provider.getCode(addrMeta.id, latestBlock.number)
    const newData = {
      isContract: code != '0x',
      checked: checked,
    }
    batch.set(addrMeta.ref, newData, { merge: true })
    addressesProcessed++
  }
  await batch.commit()

  return { ok: true, code: 200, addressesProcessed }
}

/**
 * Search across all Ethereum addresses that have interacted with the MoonCatRescue ecosystem
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const getOwnerProfiles = onRequest(async (req, res) => {
  switch (req.method) {
    case 'GET':
      let limit = 10
      let startAfterAddress: string | false = false
      let orderBy = 'id'

      // Check input parameters for errors
      let errors = []
      if (typeof req.query.limit != 'undefined') {
        if (
          typeof req.query.limit != 'string' ||
          isNaN(Number(req.query.limit)) ||
          parseInt(req.query.limit) < 1 ||
          parseInt(req.query.limit) > 500
        ) {
          errors.push('limit')
        } else {
          // Query parameter is valid; parse it
          limit = parseInt(req.query.limit)
        }
      }

      if (typeof req.query.sort != 'undefined') {
        if (typeof req.query.sort != 'string' || !['mooncats', 'moments'].includes(req.query.sort.toLowerCase())) {
          errors.push('sort')
        } else {
          // Query parameter is valid; parse it
          orderBy = req.query.sort.toLowerCase()
        }
      }

      if (typeof req.query.start_after != 'undefined') {
        if (typeof req.query.start_after != 'string') {
          errors.push('start_after')
        } else {
          startAfterAddress = req.query.start_after
        }
      }
      if (errors.length > 0) {
        res.status(400).json({ ok: false, error: 'Invalid ' + errors.join(', ') })
        return
      }

      // Query for addresses
      let query = db.collection(ADDR_META_COLLECTION).limit(limit)

      switch (orderBy) {
        case 'mooncats':
          logger.debug('Sorting by owned MoonCat count')
          query = query.orderBy('ownedMoonCats.count', 'desc').orderBy(FieldPath.documentId())
          break
        case 'moments':
          logger.debug('Sorting by owned Moments count')
          query = query.orderBy('ownedMoments.count', 'desc').orderBy(FieldPath.documentId())
          break
        default:
          query = query.orderBy(FieldPath.documentId())
      }

      // Start at a specific offset, if `start_after` query param is set
      if (startAfterAddress) {
        logger.debug(`Filter addresses starting after ${startAfterAddress}`)
        const doc = await db.collection(ADDR_META_COLLECTION).doc(startAfterAddress).get()
        query = query.startAfter(doc)
      }

      // Return result
      const snapshot = await query.get()
      res.json(
        snapshot.docs.map((addrSnap) => {
          const data = addrSnap.data()
          return {
            address: addrSnap.id,
            ownedMoonCats: data.ownedMoonCats ? data.ownedMoonCats.count : 0,
            ownedMoments: data.ownedMoments ? data.ownedMoments.count : 0,
          }
        })
      )
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})

/**
 * Retrieve information about a specific Ethereum address
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const getOwnerProfile = onRequest({ secrets: ['API_URL'] }, async (req, res) => {
  switch (req.method) {
    case 'GET':
      const address = await parseInputAddress(
        req,
        '/owner-profile/',
        new ethers.providers.JsonRpcProvider(process.env.API_URL)
      )
      if (address == null) {
        res.status(400).json({ ok: false, error: 'Bad input' })
        return
      }

      const output: {
        ownedMoonCats: MoonCatSummary[]
        ownedMoments: { momentId: number; moonCat?: number }[]
      } = {
        ownedMoonCats: [],
        ownedMoments: [],
      }

      let snapshot = await db
        .collection(ADDR_META_COLLECTION)
        .doc(address as string)
        .get()
      if (!snapshot.exists) {
        logger.info(`getOwnerProfile ADDRESS="${address}": address not found`)
        res.json(output)
        return
      }
      const addrData = snapshot.data()
      if (typeof addrData == 'undefined') {
        logger.info(`getOwnerProfile ADDRESS="${address}": address document is empty`)
        res.json(output)
        return
      }
      output.ownedMoonCats = addrData.ownedMoonCats ? (addrData.ownedMoonCats.list as MoonCatSummary[]) : []
      output.ownedMoments = addrData.ownedMoments ? addrData.ownedMoments.list : []
      logger.info(`getOwnerProfile ADDRESS="${address}": address owns ${output.ownedMoonCats.length} MoonCats`)
      logger.info(`getOwnerProfile ADDRESS="${address}": address owns ${output.ownedMoments.length} MoonCatMoments`)
      res.json(output)
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})
