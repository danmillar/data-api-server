import { getFirestore, Timestamp } from 'firebase-admin/firestore'
import { ADDR_META_COLLECTION } from './logUtils'
import { getMoonCatOwnerFromDoc, getOwnedMoonCats, MOONCAT_COLLECTION } from './moonCatUtils'
import { getPathSuffix } from './utils'

// The Cloud Functions for Firebase SDK. Used to create Cloud Functions and set up triggers.
import { onRequest } from 'firebase-functions/v2/https'
import { onDocumentWritten } from 'firebase-functions/v2/firestore'
import { logger } from 'firebase-functions'
import { parseOwnershipUpdates } from './ownerProfiles'
import { getOwnedMoments, MOMENTS_COLLECTION } from './momentUtils'

// The Firebase Admin SDK to access Firestore.
const db = getFirestore()

/**
 * Whenever a MoonCat's metadata is updated, update ownership listing.
 *
 * This is a Firebase hook function that automatically fires whenever a Firestore document matching the criteria is updated.
 */
export const onMoonCatUpdated = onDocumentWritten(`/${MOONCAT_COLLECTION}/{moonCatId}`, async (event) => {
  if (typeof event == 'undefined' || typeof event.data == 'undefined') return
  const moonCatId = event.params.moonCatId

  const ownersNeedingUpdates = await parseOwnershipUpdates(
    `MoonCat ${moonCatId}`,
    event.data.after,
    event.data.before,
    getMoonCatOwnerFromDoc
  )

  // Loop through all addresses needing updates
  for (let addr in ownersNeedingUpdates) {
    const docRef = db.collection(ADDR_META_COLLECTION).doc(addr)

    // Each individual address gets its own transaction. Because if something modifies the MoonCat records
    // this address owns, we want Firebase to re-run just that address' job
    await db.runTransaction(async (dbtx) => {
      const doc = await dbtx.get(docRef)
      let newData: { [key: string]: any } = {}
      if (!doc.exists) {
        // First time this document is being created; initialize other parameters
        newData.checked = null
      }

      // Fetch the data needed to update the document
      logger.debug(`Starting refresh of address MoonCat ownership metadata for ${addr}`)
      const ownedMoonCats = await getOwnedMoonCats({
        collection: db.collection(MOONCAT_COLLECTION),
        dbtx,
        targetAddress: addr as `0x${string}`,
      })
      newData.ownedMoonCats = {
        list: ownedMoonCats,
        count: ownedMoonCats.length,
        modified: Timestamp.now(),
      }

      logger.debug(`Starting refresh of address Moments ownership metadata for ${addr}`)
      const ownedMoments = await getOwnedMoments({
        collection: db.collection(MOMENTS_COLLECTION),
        dbtx,
        targetAddress: addr as `0x${string}`,
        ownedMoonCats: ownedMoonCats.map((m) => m.rescueOrder),
      })
      newData.ownedMoments = {
        list: ownedMoments,
        count: ownedMoments.length,
        modified: Timestamp.now(),
      }

      // Update the document
      await dbtx.set(docRef, newData, { merge: true })
    })
  }
})

/**
 * Retrieve trait information about a MoonCat.
 *
 * This is a Firebase cloud function that can be called manually by an HTTP request.
 */
export const getMoonCat = onRequest(async (req, res) => {
  switch (req.method) {
    case 'GET':
      let id = req.query.id
      if (!id || Array.isArray(id)) {
        // Fetch from path rather than query string
        id = getPathSuffix(req, '/mooncat/traits/')
        if (!id) {
          res.status(400).json({ ok: false, error: 'Bad input' })
          return
        }
      }
      logger.debug(`getMoonCat ID="${id}"`)
      let snapshot = await db
        .collection(MOONCAT_COLLECTION)
        .where('rescueOrder', '==', parseInt(id as string))
        .get()
      if (snapshot.empty) {
        snapshot = await db.collection('mooncats').where('catId', '==', id).get()
      }
      if (snapshot.empty) {
        res.status(404).json({ ok: false, error: 'Not found' })
      } else if (snapshot.docs.length > 1) {
        res.status(400).json({ ok: false, error: 'Too many found' })
      } else {
        res.json(snapshot.docs[0].data())
      }
      return
    case 'OPTIONS':
      res.setHeader('Allow', ['GET'])
      res.status(204).end()
      return
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end()
      return
  }
})
