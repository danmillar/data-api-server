
declare module '@mooncatrescue/contracts/moonCatUtils' {
  import { type Contract } from 'ethers'
  export var MoonCatRescue: Contract
  export var MoonCatAcclimator: Contract
  export var OldWrappedMoonCats: Contract
  export var JumpPort: Contract
}