require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore } = require('firebase-admin/firestore')

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

var dateOffset = 24 * 60 * 60 * 1000 * 120 // 120 days
var cutoffDate = new Date()
cutoffDate.setTime(cutoffDate.getTime() - dateOffset)

/**
 * Remove old task logs from the server
 */
;(async () => {
  console.log(`Purging logs from before ${cutoffDate.toISOString()}...`)
  for (let i = 0; i < 10; i++) {
    const snapshot = await db
      .collection('task-logs')
      .where('startTime', '<', cutoffDate)
      .orderBy('startTime', 'asc')
      .limit(400)
      .get()

    const batch = db.batch()

    if (snapshot.empty) {
      console.log('Nothing to process')
      process.exit(0)
    }

    snapshot.docs.forEach((log) => {
      const data = log.data()
      console.log(`${data.startTime.toDate().toISOString()}: ${data.taskName}`)
      batch.delete(log.ref)
    })
    await batch.commit()

    console.log(`Deleted ${snapshot.size} old logs`)
  }
})()
