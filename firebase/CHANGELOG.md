# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [6 Nov 2023]

### Added
- Created storage schema for tracking MoonCatMoments and their ownership

### Changed
- Updated `/owner-profile` endpoint to include a count of MoonCatMoments owned in the search results, and `/owner-profile/[ADDRESS]` endpoint to enumerate a summary of owned MoonCatMoments.

## [19 Sep 2023]

### Added
- Created `/owner-profile` endpoint for searching all addresses relevant to the MoonCatRescue ecosystem, and `/owner-profile/[ADDRESS]` implementation to get details on them.

### Deprecated
- `/owned-mooncats/[ADDRESS]` as the `/owner-profile/[ADDRESS]` endpoint includes that same information, and is better able to be expanded in the future.

## [26 Aug 2023]

### Added
- `GenesisCatsAdded` Events added to logged blockchain events.

## [31 July 2023]

### Added
- API endpoint `/events/` for querying blockchain events related to MoonCatRescue projects.

## [7 July 2023]

### Added
- Initial Firebase infrastructure. This includes the ability to run the Firebase Emulator Suite in a Docker container, and setup/seeding scripts to create working staging and development environments.
- Created cloud functions implementation of `/owned-mooncats/[ADDRESS]` API endpoint that returns the same data as the existing hosted version.
- Created initial cloud function implementation of `/mooncat/traits/[ID]` API endpoint. It is not identical to the existing API trait endpoint on the hosted version, and mainly serves as a debugging mechanism.
- Created Firestore data structures for tracking MoonCat traits, individual addresses as MoonCat owners, and blockchain Event data.
