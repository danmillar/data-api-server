require('dotenv').config();
const { CONFIG_FILENAME } = process.env;
if (typeof CONFIG_FILENAME === 'undefined') {
  console.error('No config file specified');
  process.exit(1);
}
const Config = require('../' + CONFIG_FILENAME);

const DELIMITER = '|';
const DEFAULT_TIMEOUT = 300; // seconds
const ITEM_FETCHING_PLACEHOLDER = '__THIS_ITEM_IS_FETCHING__';

class Item {
  constructor(type, key, value) {
    this.type = type;
    this.key = key;
    this.value = value;
    this.created = Date.now() / 1000;
    this.ttl = this._ttl();
    this.pending = false;
  }

  persist() {
    this.created = null;
  }

  setCustomTtl(seconds) {
    this.ttl = typeof seconds === 'number' ? seconds : DEFAULT_TIMEOUT;
  }

  setPending(pendingPromise) {
    this.pending = pendingPromise;
  }

  _ttl() {
    try {
      let timeout = Config.cache[this.type];

      if (typeof timeout === 'number') return timeout;
      else return DEFAULT_TIMEOUT;
    } catch (err) {
      return DEFAULT_TIMEOUT;
    }
  }
}

class Cache {
  constructor() {
    this.items = {};
  }

  _key(type, key) {
    return type + DELIMITER + key;
  }

  addItem(type, key, value) {
    this.items[this._key(type, key)] = new Item(type, key, value);
  }

  addItemAsync(type, key, pendingPromise) {
    if (!this.hasItem(type, key)) {
      // This is the first time we've ever seen this type/key before; create skeleton for it
      this.addItem(type, key, ITEM_FETCHING_PLACEHOLDER);
    }
    this.items[this._key(type, key)].setPending(pendingPromise);

    // Add a listener to convert the cached item to static, when the promise resolves
    pendingPromise
      .then(value => {
        // Fetch has resolved successfully; add the item as a static asset
        this.addItem(type, key, value)
      })
      .catch(err => {
        // Fetching the item has failed; what cleanup should we do?

        // Check if this was the first time storing this item
        if (this.items[this._key(type, key)].value == ITEM_FETCHING_PLACEHOLDER) {
          // This item failed, and we have nothing to serve up as old, so just delete it
          this.invalidate(type, key);
        } else {
          // Clear the pending promise, to allow falling back to the older value
          this.items[this._key(type, key)].setPending(false);
        }
        console.error('Cache item generation error', type, key, err);
      });
  }

  hasItem(type, key) {
    return (typeof this.items[this._key(type, key)] !== 'undefined');
  }

  isFresh(type, key) {
    if (!this.hasItem(type, key)) return false;

    let item = this.items[this._key(type, key)];

    // Check if item is persistent
    if (item.created === null) return true;

    // Otherwise, compare created date to now
    return Date.now() / 1000 <= item.created + item.ttl;
  }

  itemAge(type, key) {
    if (!this.hasItem(type, key)) return false;
    return Math.floor(Date.now() / 1000 - this.items[this._key(type, key)].created);
  }

  isPending(type, key) {
    if (!this.hasItem(type, key)) return false;
    return this.items[this._key(type, key)].pending !== false;
  }

  getItem(type, key) {
    if (!this.hasItem(type, key)) {
      // Request made for non-existent cache item
      return Promise.resolve(null);
    }
    let item = this.items[this._key(type, key)];
    if (item.pending === false) {
      // Item has no pending fetch; just serve up the saved value
      return Promise.resolve(item.value);
    } else {
      // There is a fetch already pending for this item; serve up that value when it resolves
      if (item.value === ITEM_FETCHING_PLACEHOLDER) {
        // If it fails out, we have nothing to fall back to, so just let it fail
        return item.pending;
      } else {
        // If this fetch fails, fall back to the most recent cached value
        return item.pending.catch(() => item.value);
      }
    }
  }

  setItemTtl(type, key, seconds) {
    if (this.hasItem(type, key))
      this.items[this._key(type, key)].setCustomTtl(seconds);
  }

  invalidate(type, key) {
    console.log(`Invalidating cache: ${key} (${type})`);
    delete this.items[this._key(type, key)];
  }

  clear() {
    this.items = {};
  }
}

module.exports = new Cache();
