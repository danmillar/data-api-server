
module.exports = [
  {
    label: 'Anniversary',
    start: new Date('2023-08-01T00:00:00.000Z'),
    end: new Date('2023-08-31T23:59:59.999Z'),
    accessories: (moonCat) => {
      let acc = [36]

      // Hat
      let hat = null
      if (moonCat.ownsAccessory(1215)) {
        hat = 1215 // Use 5-year hat if they own it
      } else if (moonCat.ownsAccessory(798) != 'undefined') {
        hat = 798 // Otherwise use 4-year hat if they own it
      }

      // Balloon
      acc.push(1237)

      // Cake
      if (moonCat.ownsAccessory(800)) {
        acc.push(800) // Use anniversary acclimator treat
      } else {
        // Otherwise, show whatever cupcakes they own
        acc.push(799)
        acc.push(1214)
      }

      // Extras
      acc.push(0)
      if (hat == null) {
        acc.push(2) // Show gem, if they don't have a hat already
      } else {
        acc.push(hat)
      }
      acc.push(833)

      //console.debug(`${moonCat.catId} event accessories: ${acc.join(',')}`)
      return acc.join(',')
    }
  },
  {
    label: 'Thanksgiving',
    start: new Date('2024-11-19T00:00:00.000Z'),
    end: new Date('2024-11-25T23:59:59.999Z'),
    accessories: (moonCat) => {
      let acc = [36]

      if (moonCat.ownsAccessory(1047)) {
        acc.push(1047) // Use the hat, if they own it
      } else {
        acc.push(2) // Otherwise use the gem, if they own it
      }

      acc.push(0)
      acc.push(833)

      //console.debug(`${moonCat.catId} event accessories: ${acc.join(',')}`)
      return acc.join(',')
    }
  },
  {
    label: 'Valentines',
    start: new Date('2024-02-01T00:00:00.000Z'),
    end: new Date('2024-02-28T23:59:59.999Z'),
    accessories: (moonCat) => {
      let acc = [36]

      if (moonCat.ownsAccessory(1136)) {
        acc.push(1136) // Use the hat, if they own it
      } else {
        acc.push(2) // Otherwise use the gem, if they own it
      }

      acc.push(0)
      acc.push(833)

      //console.debug(`${moonCat.catId} event accessories: ${acc.join(',')}`)
      return acc.join(',')
    }
  },
  {
    label: 'Rediscovery',
    start: new Date('2024-03-01T00:00:00.000Z'),
    end: new Date('2024-03-31T23:59:59.999Z'),
    accessories: (moonCat) => {
      let acc = [36]

      let pad = null
      if (moonCat.ownsAccessory(31) && moonCat.ownsAccessory(32)) {
        // MoonCat owns both Acclimator pads. Pick one randomly
        pad = (Math.random() > 0.5) ? 31 : 32;
      } else if (moonCat.ownsAccessory(31)) {
        pad = 31
      } else if (moonCat.ownsAccessory(32)) {
        pad = 32
      }

      let foreground = null
      if (moonCat.ownsAccessory(1168)) {
        if (pad != null) {
          // MoonCat owns both an Acclimator pad and the rocket ride. Pick one randomly
          if (Math.random() > 0.5) {
            pad = null
            foreground = 1168
          }
        } else {
          foreground = 1168
        }
      }

      if (pad) acc.push(pad)

      acc.push(1)
      acc.push(2)
      acc.push(0)
      acc.push(833)

      if (foreground) acc.push(foreground)

      //console.debug(`${moonCat.catId} event accessories: ${acc.join(',')}`)
      return acc.join(',')
    }
  },
]